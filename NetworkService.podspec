
Pod::Spec.new do |s|

    s.name             = 'NetworkService'
    s.version          = '0.0.1'
    s.summary          = 'Rx wrapper for URL Session data tasks.'

    s.description      = <<-DESC
    Rx wrapper for URL Session data tasks.
    ---
    DESC

    s.homepage     = 'https://gitlab.noveogroup.com/spletnev/RxNetworking'
    s.license      = { :type => 'MIT', :file => 'LICENSE' }
    s.author       = { 'Stanislav Pletnev' => 'stanislav.pletnev@noveogroup.com' }

    s.swift_version = '5.1'

    s.ios.deployment_target     = '10.0'
    s.tvos.deployment_target    = '10.0'
    s.osx.deployment_target     = '10.15'

    s.source       = { :git => 'https://github.com/Anobisoft/SwanKit.git', :tag => s.version.to_s }
    s.source_files  = 'NetworkService/**/*.swift'

    s.frameworks = 'Foundation'
    s.dependency 'RxSwift', '~> 5.1'

end

//
//  NetworkService.swift
//  RxNetworking
//
//  Created by Stanislav Pletnev on 2020-06-28.
//  Copyright © 2020 Stanislav Pletnev. All rights reserved.
//

import Foundation
import RxSwift

public typealias URLPath = String

public class NetworkService {

    public var baseURL: URL
    public var authToken: AuthorizationToken?
    public var defaultHeaders: [String: String]?
    
    public init(baseURL: URL, authToken: AuthorizationToken? = nil, defaultHeaders: [String: String]? = nil) {
        self.baseURL = baseURL
        self.authToken = authToken
        self.defaultHeaders = defaultHeaders
    }
    
    public func request(path: URLPath, method: HTTPMethod = .get, headers: [String: String]? = nil, contentType: MIMEType? = nil, data: Data? = nil) -> Observable<HTTPResponse> {
        
        guard let endpoint = URL(string: path, relativeTo: baseURL) else {
            return .error(NetworkService.Error.unresolvableEndpoint(base: baseURL, path: path))
        }
        
        var request = URLRequest(url: endpoint)
        request.httpMethod = method.rawValue
        request.httpBody = data
        
        request.addHeaders(defaultHeaders)
        request.addHeaders(headers)
        request.addAuthorization(token: authToken)
        
        if let contentType = contentType {
            request.addHeader(.contentType, value: contentType.description)
        }

        return .create { observer in
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                guard let response = response as? HTTPURLResponse else {
                    if let error = error {
                        observer.on(.error(error))
                    }
                    observer.on(.completed)
                    return
                }
                observer.on(.next(.init(data: data, statusCode: response.statusCode, headers: response.allHeaderFields)))
                observer.on(.completed)
            }
            task.resume()
            return Disposables.create {
                task.cancel()
            }
        }
    }
    
    public func request(path: URLPath, method: HTTPMethod = .post, headers: [String: String]? = nil, object: Encodable, encoder: JSONEncoder = JSONEncoder()) -> Observable<HTTPResponse> {
        do {
            let data = try object.encodeJSON(encoder: encoder)
            return request(path: path, method: method, headers: headers, contentType: .application(.json), data: data)
        } catch {
            return .error(error)
        }
    }
}

extension URLRequest {
    enum HeaderField: String {
        case authorization = "Authorization"
        case contentType = "Content-Type"
        case accept = "Accept"
    }

    mutating func addHeader(_ field: HeaderField, value: String) {
        addValue(value, forHTTPHeaderField: field.rawValue)
    }
    
    mutating func addHeaders(_ headers: [String: String]?) {
        guard let headers = headers else { return }
        for header in headers {
            addValue(header.value, forHTTPHeaderField: header.key)
        }
    }
}

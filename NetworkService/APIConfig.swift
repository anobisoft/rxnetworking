//
//  APIConfig.swift
//  RxNetworking
//
//  Created by Stanislav Pletnev on 2020-06-28.
//  Copyright © 2020 Stanislav Pletnev. All rights reserved.
//

import Foundation

public struct APIConfig: Decodable {
    public let baseURL: URL
    public let appUUID: UUID
    public let appToken: String
}

public extension Decodable {
    static func loadPropertyList(_ filename: String = String(describing: Self.self)) throws -> Self {
        guard
            let fileURL = Bundle.main.url(forResource: filename, withExtension: "plist")
        else {
            throw CocoaError(.fileNoSuchFile)
        }
        let data = try Data(contentsOf: fileURL)
        return try PropertyListDecoder().decode(Self.self, from: data)
    }
}

extension NetworkService {
    public convenience init(config: APIConfig) {
        let authToken = BasicAuthorization(login: config.appUUID.uuidString.lowercased(),
                                           password: config.appToken)
        self.init(baseURL: config.baseURL, authToken: authToken)
    }
}

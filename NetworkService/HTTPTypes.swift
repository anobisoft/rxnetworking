//
//  HTTPMethod.swift
//  RxNetworking
//
//  Created by Stanislav Pletnev on 2020-06-28.
//  Copyright © 2020 Stanislav Pletnev. All rights reserved.
//

import Foundation

public struct HTTPResponse {
    var data: Data?
    var statusCode: Int
    var headers: [AnyHashable: Any]?
}

public enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case patch = "PATCH"
    case delete = "DELETE"
}

public enum HTTPStatusCode: Int {
    case ok = 200
    case created = 201
    case accepted = 202
    case noContent = 204
    case resetContent = 205
    case partialContent = 206
    
    case movedPermanently = 301
    case found = 302
    case notModified = 304
    case temporaryRedirect = 307
    case permanentRedirect = 308
    
    case badRequest = 400
    case unauthorized = 401
    case paymentRequired = 402
    case forbidden = 403
    case notFound = 404
    case methodNotAllowed = 405
    case notAcceptable = 406
    case proxyAuthenticationRequired = 407
    case requestTimeout = 408
    case conflict = 409
    case gone = 410
    case lengthRequired = 411
    case preconditionFailed = 412
    case payloadTooLarge = 413
    case requestURITooLong = 414
    case unsupportedMediaType = 415
    case requestedRangeNotSatisfiable = 416
    case expectationFailed = 417
    
    case internalServerError = 500
    case notImplemented = 501
    case badGateway = 502
    case serviceUnavailable = 503
    case gatewayTimeout = 504
    case httpVersionNotSupported = 505
}

public enum MIMEType {
    case application(_ subtype: MIMEApplicationSubtype)
    case multipart(_ subtype: MIMEMultipartSubtype)
    case custom(type: String, subtype: String)
}

public enum MIMEApplicationSubtype: String {
    case json
}

public enum MIMEMultipartSubtype: String {
    case formData = "form-data"
}

extension MIMEType: CustomStringConvertible {
    
    public var description: String {
        switch self {
        case let .application(subtype):
            return formatted("application", subtype: subtype.rawValue)
            
        case let .multipart(subtype):
            return formatted("multipart", subtype: subtype.rawValue)
            
        case let .custom(type, subtype):
            return formatted(type, subtype: subtype)
            
        }
    }
    
    private func formatted(_ type: String, subtype: String) -> String {
        "\(type)/\(subtype)"
    }
}

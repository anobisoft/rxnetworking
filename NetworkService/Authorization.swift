//
//  Authorization.swift
//  RxNetworking
//
//  Created by Stanislav Pletnev on 2020-06-28.
//  Copyright © 2020 Stanislav Pletnev. All rights reserved.
//

import Foundation

public protocol AuthorizationToken {
    var value: String { get }
}

public struct BasicAuthorization: AuthorizationToken {
    public var login: String
    public var password: String
    
    public var value: String {
        let base64 = "\(login):\(password)".data(using: .utf8)!.base64EncodedString()
        return "Basic \(base64)"
    }
}

extension URLRequest {
    mutating func addAuthorization(token: AuthorizationToken?) {
        guard let token = token else { return }
        addHeader(.authorization, value: token.value)
    }
}

//
//  NetworkService.Rx+Validation.swift
//  RxNetworking
//
//  Created by Stanislav Pletnev on 2020-06-28.
//  Copyright © 2020 Stanislav Pletnev. All rights reserved.
//

import Foundation
import RxSwift

public extension NetworkService {
    enum Error: Swift.Error {
        case unresolvableEndpoint(base: URL, path: String)
        case noData
        case unacceptableStatusCode(_ code: Int, data: Data?)
    }
}

extension NetworkService.Error: LocalizedError {
    /// A textual representation of `self`, suitable for debugging.
    public var debugDescription: String {
        switch self {
        case let .unresolvableEndpoint(base, path):
            return String(format: "Unresolvable endpoint: '%@' relative to '%@'".localized, path, base.absoluteString)
        case .noData:
            return "Empty data.".localized
        case let .unacceptableStatusCode(code, data):
            let main = "Unacceptable status code: ".localized + "\(code)"
            if let data = data, let message = String(data: data, encoding: .utf8) {
                return main + " - " + message
            }
            return main
        }
    }
    
    public var errorDescription: String? {
        debugDescription
    }
}

public extension ObservableType where Element == HTTPResponse {
    func validate(statusCodes: Set<HTTPStatusCode>) -> Observable<Data> {
        map { response -> Data in
            guard let statusCode = HTTPStatusCode(rawValue: response.statusCode), statusCodes.contains(statusCode) else {
                throw NetworkService.Error.unacceptableStatusCode(response.statusCode, data: response.data)
            }
            guard let data = response.data else {
                throw NetworkService.Error.noData
            }
            return data
        }
    }
    func validate(statusCodes: Range<Int>) -> Observable<Data> {
        map { response -> Data in
            guard statusCodes.contains(response.statusCode) else {
                throw NetworkService.Error.unacceptableStatusCode(response.statusCode, data: response.data)
            }
            guard let data = response.data else {
                throw NetworkService.Error.noData
            }
            return data
        }
    }
}

private extension String {
    var localized: String {
        localized()
    }
    func localized(_ bundle: Bundle = .main, value: String? = nil, table: String? = nil) -> String {
        bundle.localizedString(forKey: self, value: value ?? self, table: table)
    }
}

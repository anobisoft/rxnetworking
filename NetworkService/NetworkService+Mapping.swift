//
//  NetworkService.Rx+Mapping.swift
//  RxNetworking
//
//  Created by Stanislav Pletnev on 2020-06-28.
//  Copyright © 2020 Stanislav Pletnev. All rights reserved.
//

import Foundation
import RxSwift

public extension ObservableType where Element == HTTPResponse {
    func decodeJSON<ModelType: Decodable>(_ type: ModelType.Type = ModelType.self,
                                          decoder: JSONDecoder = JSONDecoder()) -> Observable<ModelType> {
        map { response -> ModelType in
            guard let data = response.data else {
                throw NetworkService.Error.noData
            }
            return try decoder.decode(ModelType.self, from: data)
        }
    }
}

public extension ObservableType where Element == Data {
    func decodeJSON<ModelType: Decodable>(_ type: ModelType.Type = ModelType.self,
                                          decoder: JSONDecoder = JSONDecoder()) -> Observable<ModelType> {
        map { try decoder.decode(type.self, from: $0) }
    }
}

public extension Encodable {
    func encodeJSON(encoder: JSONEncoder = JSONEncoder()) throws -> Data {
        try encoder.encode(self)
    }
}

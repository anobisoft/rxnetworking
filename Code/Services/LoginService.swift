//
//  LoginService.swift
//  RxNetworking
//
//  Created by Stanislav Pletnev on 2020-06-30.
//  Copyright © 2020 Stanislav Pletnev. All rights reserved.
//

import Foundation
import RxSwift

protocol LoginServiceProtocol {
    func login(with account: String) -> Observable<UUID>
}

extension LoginService: LoginServiceProtocol {
    func login(with account: String) -> Observable<UUID> {
        .create { observer in
            do {
                observer.onNext(try self.login(with: account))
                observer.onCompleted()
            } catch {
                observer.onError(error)
            }
            return Disposables.create()
        }
    }
}

class LoginService {
    
    static let serviceId = "LoginService-User-UUID"
    
    let keychainAccess: KeychainAccessProtocol
    let passwordsAPIService: PasswordsAPIServiceProtocol
    lazy var passwordsCache = [String: UUID]()
    
    init(keychainAccess: KeychainAccessProtocol, passwordsAPIService: PasswordsAPIServiceProtocol) {
        self.keychainAccess = keychainAccess
        self.passwordsAPIService = passwordsAPIService
    }
    
    func login(with account: String) throws -> UUID {
        let userId: UUID
        if let cache = passwordsCache[account] {
            return cache
        }
        if let password = try keychainAccess.fetchPassword(account: account),
            let retainedID = UUID(uuidString: password) {
            userId = retainedID
        } else {
            userId = UUID()
            try keychainAccess.save(account: account, password: userId.uuidString)
        }
        passwordsAPIService.userId = userId
        passwordsCache[account] = userId
        return userId
    }
}

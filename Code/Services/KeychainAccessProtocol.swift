//
//  KeychainAccessProtocol.swift
//  RxNetworking
//
//  Created by Stanislav Pletnev on 2020-06-30.
//  Copyright © 2020 Stanislav Pletnev. All rights reserved.
//

import Foundation
import SwanKit

protocol KeychainAccessProtocol {
    func fetchPassword(account: String) throws -> String?
    func save(account: String, password: String) throws
}

extension Keychain.Service: KeychainAccessProtocol {}

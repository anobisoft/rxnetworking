//
//  PasswordsAPIService.swift
//  RxNetworking
//
//  Created by Stanislav Pletnev on 2020-06-30.
//  Copyright © 2020 Stanislav Pletnev. All rights reserved.
//

import Foundation
import RxSwift
import NetworkService

protocol PasswordsAPIServiceProtocol: class {
    // User identifier
    var userId: UUID? { get set }
    // Get all Service items
    func getServices() -> Observable<[PasswordServiceItem]>
    // Get Service by identifier
    func getService(id: String) -> Observable<PasswordServiceItem>
    // Post new Service
    func postService(_ service: PasswordServiceItem) -> Observable<Void>
    // Delete Service by identifier
    func deleteService(id: String) -> Observable<Void>
    // Patch the Service
    func patchService(_ service: PasswordServiceItem, changed: PasswordServiceItem) -> Observable<Void>
}

class PasswordsAPIService {
    
    private let networkService: NetworkServiceProtocol
    
    init(networkService: NetworkServiceProtocol) {
        self.networkService = networkService
    }
    
    func request(path: URLPath,
                 method: HTTPMethod = .get,
                 headers: [String: String]? = nil,
                 contentType: MIMEType? = nil,
                 data: Data? = nil) -> Observable<HTTPResponse> {
        networkService
            .request(path: path,
                     method: method,
                     headers: headers,
                     contentType: contentType,
                     data: data)
    }
    
    func request(path: URLPath,
                 method: HTTPMethod = .post,
                 headers: [String: String]? = nil,
                 object: Encodable,
                 encoder: JSONEncoder = JSONEncoder()) -> Observable<HTTPResponse> {
        do {
            return try networkService
                .request(path: path,
                         method: method,
                         headers: headers,
                         object: object,
                         encoder: encoder)
        } catch {
            fatalError(error.localizedDescription)
        }
    }
    
}

extension PasswordsAPIService: PasswordsAPIServiceProtocol {
    
    var userId: UUID? {
        get {
            networkService.defaultHeaders?[.userId]
        }
        set {
            if networkService.defaultHeaders == nil {
                networkService.defaultHeaders = [:]
            }
            networkService.defaultHeaders![.userId] = newValue
        }
    }
    
    func getServices() -> Observable<[PasswordServiceItem]> {
        request(path: .services)
            .validate(statusCodes: [.ok])
            .decodeJSON()
    }
    
    func getService(id: String) -> Observable<PasswordServiceItem> {
        request(path: .services(id))
            .validate(statusCodes: [.ok])
            .decodeJSON()
    }
    
    func postService(_ service: PasswordServiceItem) -> Observable<Void> {
        request(path: .services, object: service)
            .validate(statusCodes: [.noContent])
            .map { _ in }
    }
    
    func deleteService(id: String) -> Observable<Void> {
        request(path: .services(id), method: .delete)
            .validate(statusCodes: [.noContent])
            .map { _ in }
    }
    
    func patchService(_ service: PasswordServiceItem, changed: PasswordServiceItem) -> Observable<Void> {
        request(path: .services(service.id), method: .patch, object: service.changes(changed))
            .validate(statusCodes: [.noContent])
            .map { _ in }
    }
}

private extension URLPath {
    static let services = "services"
    static func services(_ id: String? = nil) -> Self {
        if let id = id {
            return services + "/" + id
        }
        return services
    }
}

private enum APISpecificHeaderField: String {
    case userId = "Passwords-User-ID"
}

private extension Dictionary where Key == String, Value == String {
    subscript(key: APISpecificHeaderField) -> UUID? {
        get {
            self[key.rawValue]?.toUUID()
        }
        set {
            self[key.rawValue] = newValue?.uuidString
        }
    }
}

extension String {
    func toUUID() -> UUID? {
        UUID(uuidString: self)
    }
}

//
//  PasswordGenerator.swift
//  RxNetworking
//
//  Created by Stanislav Pletnev on 2020-06-30.
//  Copyright © 2020 Stanislav Pletnev. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol RxPasswordGenerator {
    func generate(length: Int) -> Driver<String>
}

extension PasswordGenerator: RxPasswordGenerator {
    func generate(length: Int) -> Driver<String> {
        .just(generate(length: length))
    }
}

class PasswordGenerator {
    
    private var allowed = ClosedRange.asciiPrintable.scalars.filter {
        CharacterSet.urlPasswordAllowed.contains($0)
    }.map(Character.init)
    
    private var shuffled: [Character] {
        allowed.shuffle()
        return allowed
    }
    
    func generate(length: Int) -> String {
        .init(shuffled.prefix(length))
    }
}

private extension ClosedRange where Bound == Unicode.Scalar {
    
    static let asciiPrintable: ClosedRange = " "..."~"
    
    var range: ClosedRange<UInt32> { lowerBound.value...upperBound.value }
    var scalars: [Unicode.Scalar] { range.compactMap(Unicode.Scalar.init) }
    var characters: [Character] { scalars.map(Character.init) }
}

private extension String {
    init<S: Sequence>(_ sequence: S) where S.Element == Unicode.Scalar {
        self.init(UnicodeScalarView(sequence))
    }
}

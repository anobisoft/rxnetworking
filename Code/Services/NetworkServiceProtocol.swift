//
//  NetworkServiceProtocol.swift
//  RxNetworking
//
//  Created by Stanislav Pletnev on 2020-06-30.
//  Copyright © 2020 Stanislav Pletnev. All rights reserved.
//

import Foundation
import RxSwift
import NetworkService

protocol NetworkServiceProtocol: class {
    var defaultHeaders: [String: String]? { get set }
    
    func request(path: URLPath,
                 method: HTTPMethod,
                 headers: [String: String]?,
                 contentType: MIMEType?,
                 data: Data?) -> Observable<HTTPResponse>
    
    func request(path: URLPath,
                 method: HTTPMethod,
                 headers: [String: String]?,
                 object: Encodable,
                 encoder: JSONEncoder) throws -> Observable<HTTPResponse>
}

extension NetworkService: NetworkServiceProtocol {}

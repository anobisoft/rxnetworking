//
//  PasswordsListViewModel.swift
//  RxNetworking
//
//  Created by Stanislav Pletnev on 2020-06-07.
//  Copyright © 2020 Stanislav Pletnev. All rights reserved.
//

import RxSwift
import RxCocoa

/// A bundle of connected types, describing the `PasswordsList` module unit
enum PasswordsListUnit: UnitType {
    /// Enumeration of events which can happen inside the module.
    enum Event {
        case addNewPassword
        case editPassword(id: String)
        case error(Error)
    }

    /// Describes module inputs
    struct Input {
        let willAppear: Signal<Void>
        let pullToRefresh: Signal<Void>
        let didTapAdd: Signal<Void>
        let deleteItem: Driver<String>
        let selectItem: Driver<String>
    }

    /// Describes module output
    struct Output {
        let sections: Driver<[PasswordsSection]>
        let pullcord: Signal<Void>
    }
}

// MARK: - ViewModelType implementation

/// ViewModel type for the `PasswordsList` module
final class PasswordsListViewModel: ViewModelType {
    typealias Unit = PasswordsListUnit
    typealias Context = HasPasswordsAPI & CoordinatorsFactory
    private let context: Context
    private let router: Unit.Router
    
    private let errorTracker = ErrorTracker()

    /// Returns a newly initialized view model with the context and specified router.
    ///
    /// - Parameters:
    ///   - context: Context object presenting all necessary dependences of the
    ///              `PasswordsList` view model
    ///   - router: Object that is responsible for events of the created view model
    init(context: Context, router: Unit.Router) {
        self.context = context
        self.router = router
    }
    
    func transform(input: Unit.Input) -> Unit.Output {
        let refreshRelay = BehaviorRelay<Void>(value: ())
        let getSections = refreshRelayMap(refreshRelay)
            .map(mapItems(passwordItems:))
            .trackToDriver(errorTracker)
        
        let willAppear = input.willAppear.skip(1).map { _ in
            refreshRelay.accept(())
        }
        
        let pullToRefresh = input.pullToRefresh.map { _ in
            refreshRelay.accept(())
        }
        
        let openNewPasswordView = input.didTapAdd.map { [router] _ in
            router.play(event: .addNewPassword)
        }
        
        let deleteItem = input.deleteItem.flatMap { [context, errorTracker] id -> Signal<Void> in
            context.passwordsAPI.deleteService(id: id).trackToSignal(errorTracker)
        }.do(onNext: { _ in
            refreshRelay.accept(())
        })
        
        let editPassword = input.selectItem.map { [router] id in
            router.play(event: .editPassword(id: id))
        }.trackToSignal(errorTracker)

        let errors = errorTracker.map { [router] error in
            router.play(event: .error(error))
        }
        
        let pullcord = Signal.merge(willAppear, pullToRefresh, openNewPasswordView, deleteItem, editPassword, errors)
        
        return Unit.Output(sections: getSections, pullcord: pullcord)
    }
}

// MARK: - Private transform function decomposition

private extension PasswordsListViewModel {
    
    func refreshRelayMap(_ relay: BehaviorRelay<Void>) -> Observable<[PasswordServiceItem]> {
        relay.asObservable().flatMap({ [context] _ in
            context.passwordsAPI.getServices()
        })
    }
    
    func mapItems(passwordItems: [PasswordServiceItem]) -> [PasswordsSection] {
        let cellViewModels = passwordItems.map {
            PasswordsSectionItem(identity: $0.id, viewModel: PasswordCellViewModel(model: $0))
        }
        return [PasswordsSection(items: cellViewModels)]
    }

}

//
//  PasswordsListCoordinator.swift
//  RxNetworking
//
//  Created by Stanislav Pletnev on 2020-06-07.
//  Copyright © 2020 Stanislav Pletnev. All rights reserved.
//

import UIKit

/// Enumeration of events which can happen in the `PasswordsList` module
/// and should be handled outside.
enum PasswordsListCoordinatorEvent {
    case logout
}

/// Coordinator type for the `PasswordsList` module.
///
/// It aims to instantiate view controller and handle its inner routing.
final class PasswordsListCoordinator: CoordinatorType {
    typealias Context = PasswordsListViewModel.Context
    private let context: Context
    typealias Parent = AnyRouter<PasswordsListCoordinatorEvent>
    private let parent: Parent
    private let navigation: NavigationControllerType

    /// Returns a newly initialized coordinator with the context and specified parent router.
    ///
    /// - Parameters:
    ///   - context: Context object presenting all necessary dependences of the `PasswordsList` module
    ///   - parent: Object that is responsible for events of the created coordinator
    init(context: Context, parent: Parent, navigation: NavigationControllerType) {
        self.context = context
        self.parent = parent
        self.navigation = navigation
    }

    /// Instantiates and returns a newly initialized view controller with the nib file of the
    /// `PasswordsList` module.
    ///
    /// Also, a viewModel of the `PasswordsList` module is created and
    /// is set into the returning controller to complete **MVVM** pattern.
    /// - Returns: The view controller corresponding to the `PasswordsList` module.
    func makeInitial() -> UIViewController {
        let controller = PasswordsListViewController()
        let viewModel = PasswordsListViewModel(context: context, router: self.asRouter())

        controller.viewModel = viewModel.asViewModel()

        return controller
    }
}

// MARK: - RouterType implementation

extension PasswordsListCoordinator: RouterType {
    /// Handles the `PasswordsList` module inner events.
    ///
    /// - Parameter event: An event happened in the `PasswordsListViewModel`
    func play(event: PasswordsListUnit.Event) {
        switch event {
            
        case .addNewPassword:
            let coordinator = context.makePasswordEditCoordinator(navigation: navigation, passwordId: nil)
            let viewController = coordinator.makeInitial()
            navigation.push(viewController, animated: true)
            
        case let .editPassword(id):
            let coordinator = context.makePasswordEditCoordinator(navigation: navigation, passwordId: id)
            let viewController = coordinator.makeInitial()
            navigation.push(viewController, animated: true)
            
        case let .error(error):
            showErrorAlert(navigation: navigation, error: error)
        }
    }
}

//
//  PasswordsListViewController.swift
//  RxNetworking
//
//  Created by Stanislav Pletnev on 2020-06-07.
//  Copyright © 2020 Stanislav Pletnev. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import TinyConstraints
import Reusable
import SwipeCellKit

/// The union of all layout constants, colors, font sizes etc.
private struct Constants {
    let passwordCellId = "passwordCell"
}

private let consts = Constants()

/// UIViewController type for the `PasswordsList` module
final class PasswordsListViewController: UIViewController, ViewModelBindable {
    typealias Unit = PasswordsListUnit
    var viewModel: PasswordsListUnit.ViewModel?
    private let bag: DisposeBag = .init()
    private let tableView = UITableView()
    private let plusButton = UIBarButtonItem(barButtonSystemItem: .add, target: nil, action: nil)
    private var dataSource: RxTableViewSectionedAnimatedDataSource<PasswordsSection>!
    private let deleteRelay = PublishRelay<String>()
    private let willAppearRelay = PublishRelay<Void>()
}

// MARK: - View events

extension PasswordsListViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        precondition(viewModel != nil, "The view model must be assigned before view is loaded")

        configureView()
        bindViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        willAppearRelay.accept(())
    }
}

// MARK: - View configuration

extension PasswordsListViewController {
    /// Performs view configuration.
    func configureView() {
        view.backgroundColor = .white
        
        view.addSubview(tableView)
        tableView.refreshControl = UIRefreshControl()
        tableView.edgesToSuperview()
        tableView.register(cellType: PasswordCell.self)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 44.0
        dataSource = .init(configureCell: { [weak self] (source, tableView, indexPath, item) -> UITableViewCell in
            let cell: PasswordCell = tableView.dequeueReusableCell(for: indexPath)
            cell.viewModel = item.viewModel.asViewModel()
            cell.delegate = self
            return cell
        })
        
        navigationItem.rightBarButtonItem = plusButton
    }
}

extension PasswordsListViewController: SwipeTableViewCellDelegate {
    func tableView(_ tableView: UITableView,
                   editActionsForRowAt indexPath: IndexPath,
                   for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        
        switch orientation {
        case .right:
            let itemId = dataSource.sectionModels[indexPath.section].items[indexPath.row].identity
            let action = SwipeAction(style: .destructive, title: "delete") { [deleteRelay] (action, indexPath) in
                deleteRelay.accept(itemId)
            }
            return [action]
        default:
            return nil
        }
    }
}

// MARK: - View model binding

extension PasswordsListViewController {
    func bind(viewModel: Unit.ViewModel) {
        let pullToRefreshSignal = tableView.refreshControl!.rx.controlEvent(.valueChanged).asSignal()
        let selectItemDriver = tableView.rx.modelSelected(PasswordsSectionItem.self).asDriver().map { $0.identity }
        let deleteItemDriver = deleteRelay.asDriver(onErrorDriveWith: .never())
        let input = ViewModel.Input(willAppear: willAppearRelay.asSignal(),
                                    pullToRefresh: pullToRefreshSignal,
                                    didTapAdd: plusButton.rx.tap.asSignal(),
                                    deleteItem: deleteItemDriver,
                                    selectItem: selectItemDriver)
        let output = viewModel.transform(input: input)
        output.sections
            .map { [tableView] in tableView.refreshControl?.endRefreshing(); return $0 }
            .drive(tableView.rx.items(dataSource: dataSource))
            .disposed(by: bag)
        
        output.pullcord.emit().disposed(by: bag)
    }
}

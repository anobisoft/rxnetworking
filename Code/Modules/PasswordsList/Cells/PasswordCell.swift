//
//  PasswordCell.swift
//  RxNetworking
//
//  Created by Stanislav Pletnev on 2020-06-07.
//  Copyright © 2020 Stanislav Pletnev. All rights reserved.
//

import UIKit
import RxSwift
import TinyConstraints
import Reusable
import SwipeCellKit

final class PasswordCell: SwipeTableViewCell {
    typealias Unit = PasswordCellUnit
    
    private let serviceLabel = UILabel()
    private let urlLabel = UILabel()
    private let accountLabel = UILabel()
    private let passwordLabel = UILabel()
    
    var viewModel: Unit.ViewModel? {
        didSet {
            bindViewModel()
        }
    }
    private var bag: DisposeBag = DisposeBag()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.backgroundColor = .white
        
        let serviceStack = UIStackView(arrangedSubviews: [serviceLabel, urlLabel])
        serviceStack.axis = .horizontal
        serviceStack.spacing = 8
        
        let accountStack = UIStackView(arrangedSubviews: [accountLabel, passwordLabel])
        accountStack.axis = .horizontal
        accountStack.spacing = 8
        
        let verticalStack = UIStackView(arrangedSubviews: [serviceStack, accountStack])
        verticalStack.axis = .vertical
        verticalStack.spacing = 4
        
        contentView.addSubview(verticalStack)
        verticalStack.edgesToSuperview(insets: .uniform(8), usingSafeArea: true)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        bag = .init()
        bindViewModel()
    }
    
}

extension PasswordCell: Reusable {}

extension PasswordCell: ViewModelBindable {
    func bind(viewModel: Unit.ViewModel) {
        let input = Unit.Input()
        let output = viewModel.transform(input: input)
        output.service.drive(serviceLabel.rx.text).disposed(by: bag)
        output.serviceURL.drive(urlLabel.rx.text).disposed(by: bag)
        output.account.drive(accountLabel.rx.text).disposed(by: bag)
        output.password.drive(passwordLabel.rx.text).disposed(by: bag)
    }
}

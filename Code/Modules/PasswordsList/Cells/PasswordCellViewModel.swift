//
//  PasswordCellViewModel.swift
//  RxNetworking
//
//  Created by Stanislav Pletnev on 2020-06-07.
//  Copyright © 2020 Stanislav Pletnev. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

enum PasswordCellUnit: UnitType {
    struct Input {
        
    }
    struct Output {
        let service: Driver<String>
        let account: Driver<String>
        let serviceURL: Driver<String>
        let password: Driver<String>
    }
}

struct PasswordCellViewModel: ViewModelType {
    typealias Unit = PasswordCellUnit
    
    let service: String
    let serviceURL: String
    let account: String
    let password: String
    
    init(model: PasswordServiceItem) {
        service = model.name
        account = model.login
        password = model.password
        serviceURL = model.url.absoluteString
    }
    
    func transform(input: PasswordCellUnit.Input) -> PasswordCellUnit.Output {
        return Output(service: .just(service), account: .just(account), serviceURL: .just(serviceURL), password: .just(password))
    }
    
}

extension PasswordCellViewModel: Equatable {}

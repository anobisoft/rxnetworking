//
//  PasswordsSection.swift
//  RxNetworking
//
//  Created by Stanislav Pletnev on 2020-06-07.
//  Copyright © 2020 Stanislav Pletnev. All rights reserved.
//

import Foundation
import RxDataSources

struct PasswordsSection {
    var items: [PasswordsSectionItem]
}

extension PasswordsSection: AnimatableSectionModelType {
    
    typealias Item = PasswordsSectionItem
    
    var identity: Int {
        0
    }
    
    init(original: PasswordsSection, items: [Item]) {
        self = original
        self.items = items
    }
}

struct PasswordsSectionItem: IdentifiableType {
    var identity: String
    let viewModel: PasswordCellViewModel
}

extension PasswordsSectionItem: Equatable {}

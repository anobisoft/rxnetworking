
import RxSwift
import RxCocoa

/// A bundle of connected types, describing the `Login` module unit
enum LoginUnit: UnitType {
    /// Enumeration of events which can happen inside the module.
    enum Event {
        case success
        case error(Error)
    }

    /// Describes module inputs
    struct Input {
        let login: Driver<String>
        let didTapSubmit: Signal<Void>
    }

    /// Describes module output
    struct Output {
        let submitButtonIsActive: Driver<Bool>
        let pullcord: Signal<Void>
    }
}

// MARK: - ViewModelType implementation

/// ViewModel type for the `Login` module
final class LoginViewModel: ViewModelType {
    typealias Unit = LoginUnit
    typealias Context = HasLoginService
    private let context: Context
    private let router: Unit.Router
    
    private let errorTracker = ErrorTracker()

    /// Returns a newly initialized view model with the context and specified router.
    ///
    /// - Parameters:
    ///   - context: Context object presenting all necessary dependences of the
    ///              `Login` view model
    ///   - router: Object that is responsible for events of the created view model
    init(context: Context, router: Unit.Router) {
        self.context = context
        self.router = router
    }

    func transform(input: Unit.Input) -> Unit.Output {
        
        let loginNotEmpty = input.login.map { !$0.isEmpty }
        
        let didTapSubmit = input.didTapSubmit
            .withLatestFrom(input.login.map { $0.trimmingCharacters(in: .whitespacesAndNewlines) })
            .flatMap(submitMap(login:))
        
        let loginError = errorTracker.map { [router] error -> Void in
            router.play(event: .error(error))
        }
        
        let pullcord = Signal.merge(loginError, didTapSubmit)
        
        return .init(submitButtonIsActive: loginNotEmpty, pullcord: pullcord)
    }
}

// MARK: - Private transform function decomposition

private extension LoginViewModel {
    func submitMap(login: String) -> Signal<Void> {
        context.loginService.login(with: login)
            .trackToSignal(errorTracker)
            .map { [router] _ in
                router.play(event: .success)
        }
    }
}

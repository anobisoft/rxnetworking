
import UIKit
import RxSwift
import RxCocoa
import TinyConstraints

/// The union of all layout constants, colors, font sizes etc.
private struct Constants {
    
}
private let consts = Constants()

/// UIViewController type for the `Login` module
final class LoginViewController: UIViewController, ViewModelBindable {
    typealias Unit = LoginUnit
    var viewModel: LoginUnit.ViewModel?
    private var usernameTextField = UITextField()
//    private var passwordTextField = UITextField()
    private var submitButton = UIButton(type: .system)
    
    private let bag: DisposeBag = .init()
}

// MARK: - View events

extension LoginViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        precondition(viewModel != nil, "The view model must be assigned before view is loaded")

        configureView()
        bindViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        usernameTextField.becomeFirstResponder()
    }
}

// MARK: - View configuration

extension LoginViewController {
    /// Performs view configuration.
    func configureView() {
        view.backgroundColor = .white
        
        usernameTextField.borderStyle = .roundedRect
        usernameTextField.placeholder = " username"
        
//        passwordTextField.borderStyle = .roundedRect
//        passwordTextField.isEnabled = false
        
        submitButton.setTitle("submit", for: .normal)
        submitButton.layer.cornerRadius = 4
        
        let stackView = UIStackView(arrangedSubviews: [usernameTextField, submitButton])
        stackView.axis = .vertical
        view.addSubview(stackView)
        stackView.edgesToSuperview(excluding: .bottom,
                                   insets: .top(200) + .horizontal(20),
                                   usingSafeArea: true)
    }

}

// MARK: - View model binding

extension LoginViewController {
    func bind(viewModel: Unit.ViewModel) {
        
        let input = ViewModel.Input(login: usernameTextField.driver,
                                    didTapSubmit: submitButton.rx.tap.asSignal())
        
        let output = viewModel.transform(input: input)
        output.submitButtonIsActive
            .drive(submitButton.rx.isEnabled)
            .disposed(by: bag)
        output.pullcord
            .emit()
            .disposed(by: bag)
    }
}



import UIKit

/// Enumeration of events which can happen in the `Login` module
/// and should be handled outside.
enum LoginCoordinatorEvent {
    case success
}

/// Coordinator type for the `Login` module.
///
/// It aims to instantiate view controller and handle its inner routing.
final class LoginCoordinator: CoordinatorType {
    typealias Context = LoginViewModel.Context
    private let context: Context
    typealias Parent = AnyRouter<LoginCoordinatorEvent>
    private let parent: Parent
    private let navigation: NavigationControllerType
    /// Returns a newly initialized coordinator with the context and specified parent router.
    ///
    /// - Parameters:
    ///   - context: Context object presenting all necessary dependences of the `Login` module
    ///   - parent: Object that is responsible for events of the created coordinator
    init(context: Context, parent: Parent, navigation: NavigationControllerType) {
        self.context = context
        self.parent = parent
        self.navigation = navigation
    }

    /// Instantiates and returns a newly initialized view controller with the nib file of the
    /// `Login` module.
    ///
    /// Also, a viewModel of the `Login` module is created and
    /// is set into the returning controller to complete **MVVM** pattern.
    /// - Returns: The view controller corresponding to the `Login` module.
    func makeInitial() -> UIViewController {
        let controller = LoginViewController()
        let viewModel = LoginViewModel(context: context, router: self.asRouter())

        controller.viewModel = viewModel.asViewModel()

        return controller
    }
}

// MARK: - RouterType implementation

extension LoginCoordinator: RouterType {
    /// Handles the `Login` module inner events.
    ///
    /// - Parameter event: An event happened in the `LoginViewModel`
    func play(event: LoginUnit.Event) {
        switch event {
        case .success:
            parent.play(event: .success)
        case let .error(error):
            showErrorAlert(navigation: navigation, error: error)
        }        
    }
}

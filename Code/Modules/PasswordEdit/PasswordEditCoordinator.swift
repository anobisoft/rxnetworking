//
//  PasswordEditCoordinator.swift
//  RxNetworking
//
//  Created by Stanislav Pletnev on 2020-06-08.
//  Copyright © 2020 Stanislav Pletnev. All rights reserved.
//

import UIKit

/// Enumeration of events which can happen in the `PasswordEdit` module
/// and should be handled outside.
enum PasswordEditCoordinatorEvent {
    case success
}

/// Coordinator type for the `PasswordEdit` module.
///
/// It aims to instantiate view controller and handle its inner routing.
final class PasswordEditCoordinator: CoordinatorType {
    typealias Context = PasswordEditViewModel.Context
    private let context: Context
    private let navigation: NavigationControllerType
    private let passwordId: String?
    
    /// Returns a newly initialized coordinator with the context and specified parent router.
    ///
    /// - Parameters:
    ///   - context: Context object presenting all necessary dependences of the `PasswordEdit` module
    ///   - parent: Object that is responsible for events of the created coordinator
    init(context: Context, navigation: NavigationControllerType, passwordId: String?) {
        self.context = context
        self.navigation = navigation
        self.passwordId = passwordId
    }

    /// Instantiates and returns a newly initialized view controller with the nib file of the
    /// `PasswordEdit` module.
    ///
    /// Also, a viewModel of the `PasswordEdit` module is created and
    /// is set into the returning controller to complete **MVVM** pattern.
    /// - Returns: The view controller corresponding to the `PasswordEdit` module.
    func makeInitial() -> UIViewController {
        let controller = PasswordEditViewController()
        let viewModel = PasswordEditViewModel(context: context, router: self.asRouter(), passwordId: passwordId)

        controller.viewModel = viewModel.asViewModel()

        return controller
    }
}

// MARK: - RouterType implementation

extension PasswordEditCoordinator: RouterType {
    /// Handles the `PasswordEdit` module inner events.
    ///
    /// - Parameter event: An event happened in the `PasswordEditViewModel`
    func play(event: PasswordEditUnit.Event) {
        switch event {
        case .saveSuccess:
            navigation.pop(animated: true)
        case let .error(error):
            showErrorAlert(navigation: navigation, error: error)
        }        
    }
}

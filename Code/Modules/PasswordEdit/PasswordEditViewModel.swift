//
//  PasswordEditViewModel.swift
//  RxNetworking
//
//  Created by Stanislav Pletnev on 2020-06-08.
//  Copyright © 2020 Stanislav Pletnev. All rights reserved.
//

import RxSwift
import RxCocoa
import RxRelay

/// A bundle of connected types, describing the `PasswordEdit` module unit
enum PasswordEditUnit: UnitType {
    /// Enumeration of events which can happen inside the module.
    enum Event {
        case saveSuccess
        case error(Error)
    }

    /// Describes module inputs
    struct Input {
        var serviceName: Driver<String>
        var serviceUrl: Driver<String>
        var account: Driver<String>
        var sliderValue: Driver<Int>
        var sliderEndEditing: Signal<Void>
        var generate: Signal<Void>
        var didTapSave: Signal<Void>
    }

    /// Describes module output
    struct Output {
        var serviceName: Driver<String>
        var serviceUrl: Driver<String>
        var account: Driver<String>
        var passwordLength: Driver<Int>
        var password: Driver<String>
        let pullcord: Signal<Void>
    }
}

// MARK: - ViewModelType implementation

/// ViewModel type for the `PasswordEdit` module
final class PasswordEditViewModel: ViewModelType {
    typealias Unit = PasswordEditUnit
    typealias Context = HasPasswordsAPI & HasPasswordGenerator
    private let context: Context
    private let router: Unit.Router
    
    private let errorTracker = ErrorTracker()
    
    private let passwordId: String?

    /// Returns a newly initialized view model with the context and specified router.
    ///
    /// - Parameters:
    ///   - context: Context object presenting all necessary dependences of the
    ///              `PasswordEdit` view model
    ///   - router: Object that is responsible for events of the created view model
    init(context: Context, router: Unit.Router, passwordId: String?) {
        self.context = context
        self.router = router
        self.passwordId = passwordId
    }

    func transform(input: Unit.Input) -> Unit.Output {
        
        let getService = Driver.just(passwordId)
            .compactMap { $0 }
            .flatMap(getPassword)
//            .debug()
        
        let mapServiceName = getService.map { $0.name }
        let mapServiceUrl = getService.map { $0.url.absoluteString }
        let mapAccount = getService.map { $0.login }
        let mapPassword = getService.map { $0.password }.asObservable()
        
        let generate = sliderMap(signal: input.generate, value: input.sliderValue)
        let sliderGenerate = sliderMap(signal: input.sliderEndEditing, value: input.sliderValue)
        let passwordCombined = Observable.merge(mapPassword, generate, sliderGenerate)
            .trackToDriver(errorTracker)
        
        let passwordLengthCombined = Observable
            .merge(.just(12),
                   mapPassword.map { $0.count },
                   input.sliderValue.asObservable())
            .trackToDriver(errorTracker)
//            .debug()
        
        let combinedFileds = Driver.combineLatest(
            Driver.just(passwordId ?? UUID().uuidString),
            Driver.merge(mapServiceName, input.serviceName),
            Driver.merge(mapServiceUrl, input.serviceUrl).compactMap(URL.init).debug(),
            Driver.merge(mapAccount, input.account),
            passwordCombined
        ).debug()
        
        let save = input.didTapSave
            .withLatestFrom(combinedFileds)
            .map(PasswordServiceItem.init)
            .asDriver(onErrorDriveWith: .never())
//            .debug()
        
        let getOrigin = input.didTapSave
            .withLatestFrom(getService)
            .asDriver(onErrorDriveWith: .never())
//            .debug()
        
        let patch = Driver.combineLatest(getOrigin, save)
            .flatMap(patchPassword)
            .map(routerSuccess(_:))
        
        let isNewPassword = Driver.just(passwordId).filter { $0 == nil }
            .asDriver(onErrorDriveWith: .never())
//            .debug()
        
        let postNew = Driver.combineLatest(isNewPassword, save)
            .flatMap(postPassword)
            .map(routerSuccess(_:))
        
        let errors = errorTracker.map(routerError(_:))
        
        let pullcord = Signal.merge(patch, postNew, errors).debug()
        
        return .init(serviceName: mapServiceName,
                     serviceUrl: mapServiceUrl,
                     account: mapAccount,
                     passwordLength: passwordLengthCombined,
                     password: passwordCombined,
                     pullcord: pullcord)
    }
}

// MARK: - Private transform function decomposition

private extension PasswordEditViewModel {
    
    func getPassword(passwordId: String) -> Driver<PasswordServiceItem> {
        context.passwordsAPI.getService(id: passwordId).trackToDriver(errorTracker)
    }
    
    func sliderMap(signal: Signal<Void>, value: Driver<Int>) -> Observable<String> {
        signal.withLatestFrom(value)
            .flatMap(context.passwordGenerator.generate(length:))
            .asObservable()
    }
    
    func routerSuccess(_ : Void) {
        router.play(event: .saveSuccess)
    }
    
    func routerError(_ error: Error) {
        router.play(event: .error(error))
    }
    
    func patchPassword(_ origin: PasswordServiceItem, changed: PasswordServiceItem) -> Signal<Void> {
        context.passwordsAPI.patchService(origin, changed: changed).trackToSignal(errorTracker)
    }
    
    func postPassword(_ passwordId: String? = nil, new: PasswordServiceItem) -> Signal<Void> {
        context.passwordsAPI.postService(new).trackToSignal(errorTracker)
    }
}

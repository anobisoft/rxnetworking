//
//  PasswordEditViewController.swift
//  RxNetworking
//
//  Created by Stanislav Pletnev on 2020-06-08.
//  Copyright © 2020 Stanislav Pletnev. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import TinyConstraints

/// The union of all layout constants, colors, font sizes etc.
private struct Constants {
    enum Placeholder: String {
        case serviceName = "Service Name"
        case serviceUrl = "Service URL"
        case account = "Account"
        case password = "Password"
    }
}
private let consts = Constants()

/// UIViewController type for the `PasswordEdit` module
final class PasswordEditViewController: UIViewController, ViewModelBindable {
    typealias Unit = PasswordEditUnit
    var viewModel: PasswordEditUnit.ViewModel?
    
    private let saveButton = UIBarButtonItem(barButtonSystemItem: .save, target: nil, action: nil)

    private let serviceNameTF = UITextField(label: .serviceName)
    private let serviceUrlTF = UITextField(label: .serviceUrl)
    private let accountTF = UITextField(label: .account)
    private let passwordSlider = UISlider()
    private let passwordLengthLabel = UILabel()
    private let generateButton = UIButton(type: .system)
    private let passwordLabel = UILabel()
    
    private let bag: DisposeBag = .init()
}

// MARK: - View events

extension PasswordEditViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        precondition(viewModel != nil, "The view model must be assigned before view is loaded")

        configureView()
        bindViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        serviceNameTF.becomeFirstResponder()
    }
}

// MARK: - View configuration

extension PasswordEditViewController {
    /// Performs view configuration.
    func configureView() {
        view.backgroundColor = .white
        navigationItem.rightBarButtonItem = saveButton
        
        serviceUrlTF.keyboardType = .URL
        serviceUrlTF.autocapitalizationType = .none
        
        passwordSlider.minimumValue = 5
        passwordSlider.maximumValue = 20
        generateButton.setTitle("generate", for: .normal)
        
        let passwordControl = UIStackView(arrangedSubviews: [passwordSlider, passwordLengthLabel, generateButton])
        passwordControl.axis = .horizontal
        passwordControl.spacing = 8
        
        let formFields = [serviceNameTF, serviceUrlTF, accountTF, passwordControl, passwordLabel]
        let formView = UIStackView(arrangedSubviews: formFields)
        formView.axis = .vertical
        formView.spacing = 8
        view.addSubview(formView)
        formView.edgesToSuperview(excluding: .bottom, insets: .horizontal(20) + .top(100))
    }

}

// MARK: - View model binding

extension PasswordEditViewController {
    func bind(viewModel: Unit.ViewModel) {
        let input = ViewModel.Input(
            serviceName: serviceNameTF.driver,
            serviceUrl: serviceUrlTF.driver,
            account: accountTF.driver,
            sliderValue: passwordSlider.rx.value.asDriver().map(Int.init),
            sliderEndEditing: passwordSlider.rx.controlEvent(.touchUpInside).asSignal(),
            generate: generateButton.rx.tap.asSignal(),
            didTapSave: saveButton.rx.tap.asSignal()
        )
        let output = viewModel.transform(input: input)
        output.serviceName.drive(serviceNameTF.rx.text).disposed(by: bag)
        output.serviceUrl.drive(serviceUrlTF.rx.text).disposed(by: bag)
        output.account.drive(accountTF.rx.text).disposed(by: bag)
        output.passwordLength.map(Float.init).drive(passwordSlider.rx.value).disposed(by: bag)
        output.passwordLength.map(String.init).drive(passwordLengthLabel.rx.text).disposed(by: bag)
        output.password.drive(passwordLabel.rx.text).disposed(by: bag)
        output.pullcord.emit().disposed(by: bag)
    }
}

// MARK: - Helpers

private extension UITextField {
    convenience init(label: Constants.Placeholder) {
        self.init()
        borderStyle = .roundedRect
        placeholder = label.rawValue
    }
}


/// Append typealias for type that should route its events
extension UnitType {
    typealias Router = AnyRouter<Event>
}

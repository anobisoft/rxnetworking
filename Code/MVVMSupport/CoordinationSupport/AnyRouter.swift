/// A type-erased router object.
final class AnyRouter<Event>: RouterType {
    private let _playEvent: (Event) -> Void

    /// Construct a router using a RouterType-bound instance.
    init<C>(_ coordinator: C) where C: RouterType,
        C.Event == Event {
        self._playEvent = coordinator.play(event:)
    }

    /// Construct a router using only an event handler function.
    init(play: @escaping (Event) -> Void) {
        self._playEvent = play
    }

    func play(event: Event) {
        _playEvent(event)
    }
}

extension RouterType {
    /// Type-erase any RouterType-compliant instance.
    func asRouter() -> AnyRouter<Event> {
        return .init(self)
    }
}

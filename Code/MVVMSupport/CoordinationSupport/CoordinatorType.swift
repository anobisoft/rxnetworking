import class UIKit.UIViewController

/// This is a protocol for an object, designed to build and connect a sytem of
/// view controllers, view models, navigation controllers, and models.  The only
/// required use is being able to install and run itself.
protocol CoordinatorType {
    /// Make initial view controller for a section.
    func makeInitial() -> UIViewController
}

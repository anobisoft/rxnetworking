import RxSwift
import RxCocoa

extension SharedSequenceConvertibleType {
    /// Pass the value to the specified router and discard it (replacing with Void()).
    func route<R>(with router: R) -> SharedSequence<SharingStrategy, Void>
        where R: RouterType, Element == R.Event {
        return self.asSharedSequence().do(onNext: router.play(event:))
            .map({ _ in () })
    }
}

extension AnyRouter {
    var input: Binder<Event> {
        return Binder<Event>(self) { coordinator, event in
            coordinator.play(event: event)
        }
    }
}

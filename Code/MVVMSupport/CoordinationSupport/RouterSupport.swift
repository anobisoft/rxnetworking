/// A router definition.
///
/// A router must be able to handle various events, transferred by other objects (normally by view models).
protocol RouterType {
    associatedtype Event

    /// Handle a coordinator event.
    func play(event: Event)
}

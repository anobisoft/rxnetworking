/// A bundle of connected types, describing a unit: input, output, events.
///
/// A unit has no active logic.
protocol UnitType {
    associatedtype Input
    associatedtype Output
    associatedtype Event = Never
}

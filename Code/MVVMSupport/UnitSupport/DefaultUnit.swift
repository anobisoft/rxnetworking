enum DefaultUnit<I, O, E>: UnitType {
    typealias Input = I
    typealias Output = O
    typealias Event = E
}

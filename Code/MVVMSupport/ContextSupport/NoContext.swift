import Foundation

/// A completely empty context.  Use it to indicate intention to has no context,
/// while maintaining similarity between context-dependent components.
protocol NoContext {
}

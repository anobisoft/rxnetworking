import UIKit

/// An abstraction of an object, capable of performing modifications to UI object composition.
protocol NavigationControllerType: ModalNavigationControllerType,
    StackNavigationControllerType,
    SingleNavigationControllerType,
    ListNavigationControllerType {
}

import class UIKit.UIViewController

/// Stack-based navigation controller protocol.  It supports pushing and popping an
/// arbitrary number of controllers.
protocol StackNavigationControllerType: AnyObject {
    typealias Item = UIViewController

    func push(_ item: Item, animated: Bool, completion: (() -> Void)?)
    func pop(animated: Bool, completion: (() -> Void)?)
    func setNavigationBarHidden(_ hidden: Bool, animated: Bool)
}

// MARK: - Provides behaviour

extension StackNavigationControllerType {
    func push(_ item: Item, animated: Bool) {
        push(item, animated: animated, completion: .none)
    }
    func pop(animated: Bool) {
        pop(animated: animated, completion: .none)
    }
}

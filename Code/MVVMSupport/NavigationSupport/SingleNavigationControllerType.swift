import class UIKit.UIViewController

/// A navigation controller, holding only a single item at a time.  On pushing an item,
/// the previous item will be evicted.
protocol SingleNavigationControllerType: AnyObject {
    typealias Item = UIViewController
    func put(_ item: Item)
}

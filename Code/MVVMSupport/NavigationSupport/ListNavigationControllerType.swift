import class UIKit.UIViewController

protocol ListNavigationControllerType: AnyObject {
    typealias Item = UIViewController
    
    var items: [Item] { get }
    func navigate(to: [Item], animated: Bool, completion: (() -> Void)?)
}

extension ListNavigationControllerType {
    /// Navigate to the list of items.
    func navigate(to items: [Item], animated: Bool) {
        navigate(to: items, animated: animated, completion: .none)
    }
}

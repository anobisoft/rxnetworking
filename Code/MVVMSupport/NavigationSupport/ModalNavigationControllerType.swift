import class UIKit.UIViewController

/// Modal presentation-based navigation controller protocol.
protocol ModalNavigationControllerType: AnyObject {
    typealias Item = UIViewController

    func present(_ item: Item, animated: Bool, completion: (() -> Void)?)
    func dismiss(animated: Bool, completion: (() -> Void)?)
    func dismissFullPresentedStack(completion: (() -> Void)?)
    var topPresentedItem: ModalNavigationControllerType? { get }
}

extension ModalNavigationControllerType {
    func present(_ item: Item, animated: Bool) {
        present(item, animated: animated, completion: .none)
    }

    func dismiss(_ animated: Bool) {
        topPresentedItem?.dismiss(animated: animated, completion: .none)
    }
}

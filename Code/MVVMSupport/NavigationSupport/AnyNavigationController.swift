final class AnyNavigationController: NavigationControllerType {
    
    private let _box: NavigationControllerType

    init(_ controller: NavigationControllerType) {
        self._box = controller
    }
    
    var topPresentedItem: ModalNavigationControllerType? {
        return _box.topPresentedItem
    }
    
    var items: [Item] {
        return _box.items
    }

    func dismissFullPresentedStack(completion: (() -> Void)?) {
        _box.dismissFullPresentedStack(completion: completion)
    }

    func setNavigationBarHidden(_ hidden: Bool, animated: Bool) {
        _box.setNavigationBarHidden(hidden, animated: animated)
    }
    
    func navigate(to items: [Item], animated: Bool,
        completion: (() -> Void)?) {
        _box.navigate(to: items, animated: animated, completion: completion)
    }

    func push(_ item: Item, animated: Bool,
        completion: (() -> Void)?) {
        _box.push(item, animated: animated, completion: completion)
    }
    
    func present(_ item: Item, animated: Bool,
        completion: (() -> Void)?) {
        _box.present(item, animated: animated, completion: completion)
    }

    func pop(animated: Bool, completion: (() -> Void)?) {
        _box.pop(animated: animated, completion: completion)
    }
    
    func dismiss(animated: Bool, completion: (() -> Void)?) {
        _box.dismiss(animated: animated, completion: completion)
    }

    func put(_ item: SingleNavigationControllerType.Item) {
        _box.put(item)
    }
}

extension NavigationControllerType {
    func asNavigationController() -> AnyNavigationController {
        return .init(self)
    }
}

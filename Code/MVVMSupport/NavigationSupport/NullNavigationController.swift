/// An "empty" navigation controller that does nothing at all.
final class NullNavigationController: NavigationControllerType {
    var items: [Item] { return [] }

    func dismissFullPresentedStack(completion: (() -> Void)?) {
    }
    
    func setNavigationBarHidden(_ hidden: Bool, animated: Bool) {
    }
    
    var topPresentedItem: ModalNavigationControllerType? {
        return nil
    }
    
    func present(_ item: ModalNavigationControllerType.Item, animated: Bool, completion: (() -> Void)?) {
    }

    func dismiss(animated: Bool, completion: (() -> Void)?) {
    }

    func push(_ item: StackNavigationControllerType.Item, animated: Bool, completion: (() -> Void)?) {
    }

    func pop(animated: Bool, completion: (() -> Void)?) {
    }

    func put(_ item: SingleNavigationControllerType.Item) {
    }

    func navigate(to: [ListNavigationControllerType.Item], animated: Bool, completion: (() -> Void)?) {
    }
}

import class UIKit.UIViewController

extension UIViewController: ModalNavigationControllerType {
    func dismissFullPresentedStack(completion: (() -> Void)?) {
        var item = self
        guard item.presentingViewController != nil || item.presentedViewController != nil else {
            completion?()
            return
        }
        while let presented = item.presentedViewController {
            item = presented
        }
        while let presenting = item.presentingViewController {
            item = presenting
            if item.presentingViewController != nil {
                item.dismiss(false, completion: nil)
            } else {
                item.dismiss(false, completion: completion)
            }
        }
    }
    
    func dismiss(_ animated: Bool, completion: (() -> Void)?) {
        presentedViewController?.dismiss(animated: animated, completion: completion)
    }
    
    var topPresentedItem: ModalNavigationControllerType? {
        var item = self
        while let presented = item.presentedViewController {
            item = presented
        }
        return item.navigationController ?? item
    }
}

import Foundation

/// Describes a case when the controller don't need any routing.
protocol EmptyNavigationType: AnyObject {
}

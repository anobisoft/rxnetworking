protocol ViewModelBindable {
    associatedtype ViewModel
    var viewModel: ViewModel? { get }
    
    /// Automates communication between the view and its bound properties in the view model.
    ///
    /// - Parameter viewModel: Abstraction of the view exposing public properties and commands.
    func bind(viewModel: ViewModel)
}

extension ViewModelBindable {
    /// Binds viewModel if it exists.
    func bindViewModel() {
        if let viewModel = viewModel {
            bind(viewModel: viewModel)
        }
    }
}

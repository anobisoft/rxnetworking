/// Core definition of a view model.
///
/// A view model must be able to do transformation of some input to output,
/// normally from view and to views. */
protocol ViewModelType {
    /// This default definition not work in Swift 4.1 (https://bugs.swift.org/browse/SR-6516):
    associatedtype Unit: UnitType = DefaultUnit<Input, Output, Never>
        where Unit.Input == Input, Unit.Output == Output
    /// associatedtype Unit: UnitType where Unit.Input == Input, Unit.Output == Output
    associatedtype Input
    associatedtype Output

    /// Prepares unit.output by the given unit.input.
    ///
    /// - Parameter input: Input can be understood as inputs, obtained from views
    /// - Returns: Output is something to be presented in a view
    func transform(input: Input) -> Output
}

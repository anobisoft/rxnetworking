import Foundation
import RxSwift

class CommonContext {
    let passwordsAPI: PasswordsAPIServiceProtocol
    let loginService: LoginServiceProtocol
    let passwordGenerator: RxPasswordGenerator

    init(passwordsAPI: PasswordsAPIServiceProtocol,
         loginService: LoginServiceProtocol,
         passwordGenerator: RxPasswordGenerator) {
        
        self.passwordsAPI = passwordsAPI
        self.loginService = loginService
        self.passwordGenerator = passwordGenerator
    }
}

extension CommonContext: NoContext {}

protocol HasPasswordsAPI {
    var passwordsAPI: PasswordsAPIServiceProtocol { get }
}

protocol HasLoginService {
    var loginService: LoginServiceProtocol { get }
}

protocol HasPasswordGenerator {
    var passwordGenerator: RxPasswordGenerator { get }
}

extension CommonContext: HasPasswordsAPI, HasLoginService, HasPasswordGenerator {}

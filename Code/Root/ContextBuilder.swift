
import Foundation
import NetworkService
import SwanKit

/// This class should configure all required services and inject them into a Context
class ContextBuilder {
    func buildContext() -> CommonContext {
        do {
            let config = try APIConfig.loadPropertyList()
            let networkService = NetworkService(config: config)
            let keychainService = Keychain.Service(id: LoginService.serviceId)
            let passwordsAPI = PasswordsAPIService(networkService: networkService)
            let loginService = LoginService(keychainAccess: keychainService,
                                            passwordsAPIService: passwordsAPI)
            return .init(passwordsAPI: passwordsAPI,
                         loginService: loginService,
                         passwordGenerator: PasswordGenerator())
        } catch {
            fatalError(error.localizedDescription)
        }
    }
}

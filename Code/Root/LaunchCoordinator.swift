
import UIKit
import RxSwift

/// A bundle of connected types, describing the `Launch` module unit
enum LaunchUnit {
    /// Enumeration of events which can happen inside the module.
    enum Event {
        case login
        case servicesList
    }
}

/// LaunchCoordinator decides which module should be shown after the application launch.
final class LaunchCoordinator: CoordinatorType {
    typealias Event = LaunchUnit.Event
    
    typealias Context = CoordinatorsFactory & HasPasswordsAPI & HasLoginService
    private let context: Context
    
    private let disposeBag = DisposeBag()
    
    private weak var navigation: SingleNavigationControllerType?

    init(context: Context, navigation: SingleNavigationControllerType) {
        self.context = context
        self.navigation = navigation
    }

    func makeInitial() -> UIViewController {
        play(event: .login)
        return UIViewController()
    }
}

// MARK: - RouterType implementation

extension LaunchCoordinator: RouterType {
    func play(event: LaunchCoordinator.Event) {
        switch event {
        case .login:
            let router = AnyRouter(play: playLogin(event:))
            let stackController = UINavigationController()
            let coordinator = context.makeLoginCoordinator(router: router, navigation: stackController)
            let initialController = coordinator.makeInitial()
            stackController.viewControllers = [initialController]
            navigation?.put(stackController)
        case .servicesList:
            let router = AnyRouter(play: playPasswordsList(event:))
            let stackController = UINavigationController()
            let coordinator = context.makePasswordsListCoordinator(router: router, navigation: stackController)
            let initialController = coordinator.makeInitial()
            stackController.viewControllers = [initialController]
            navigation?.put(stackController)
        }
    }
}

// MARK: - Routing

private extension LaunchCoordinator {
    /// Handles events from the `Login` module
    ///
    /// - Parameter event: Event of the `Login` module
    func playLogin(event: LoginCoordinatorEvent) {
        switch event {
        case .success:
            play(event: .servicesList)
        }
    }
    /// Handles events from the `PasswordsList` module
    ///
    /// - Parameter event: Event of the `PasswordsList` module
    func playPasswordsList(event: PasswordsListCoordinatorEvent) {
        switch event {
        case .logout:
            play(event: .login)
        }
    }
}

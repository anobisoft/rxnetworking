//
//  CoordinatorsFactory.swift
//  RxNetworking
//
//  Created by Stanislav Pletnev on 2020-07-06.
//  Copyright © 2020 Stanislav Pletnev. All rights reserved.
//

import Foundation

protocol CoordinatorsFactory {
    
    func makeLaunchCoordinator(navigation: SingleNavigationController) -> CoordinatorType
    func makeLoginCoordinator(router: AnyRouter<LoginCoordinatorEvent>, navigation: NavigationControllerType) -> CoordinatorType
    func makePasswordsListCoordinator(router: AnyRouter<PasswordsListCoordinatorEvent>, navigation: NavigationControllerType) -> CoordinatorType
    func makePasswordEditCoordinator(navigation: NavigationControllerType, passwordId: String?) -> CoordinatorType
}

extension CommonContext: CoordinatorsFactory {

    func makeLaunchCoordinator(navigation: SingleNavigationController) -> CoordinatorType {
        LaunchCoordinator(context: self, navigation: navigation)
    }
    func makeLoginCoordinator(router: AnyRouter<LoginCoordinatorEvent>, navigation: NavigationControllerType) -> CoordinatorType {
        LoginCoordinator(context: self, parent: router, navigation: navigation)
    }
    func makePasswordsListCoordinator(router: AnyRouter<PasswordsListCoordinatorEvent>, navigation: NavigationControllerType) -> CoordinatorType {
        PasswordsListCoordinator(context: self, parent: router, navigation: navigation)
    }
    func makePasswordEditCoordinator(navigation: NavigationControllerType, passwordId: String? = nil) -> CoordinatorType {
        PasswordEditCoordinator(context: self, navigation: navigation, passwordId: passwordId)
    }
}

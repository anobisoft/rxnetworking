
import RxSwift
import RxCocoa

extension UITextField {
    var driver: Driver<String> {
        rx.text.orEmpty.asDriver()
    }
}

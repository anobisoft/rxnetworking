//
//  Router+Alert.swift
//  RxNetworking
//
//  Created by Stanislav Pletnev on 2020-07-08.
//  Copyright © 2020 Stanislav Pletnev. All rights reserved.
//

import UIKit

extension RouterType {
    func showErrorAlert(navigation: NavigationControllerType, error: Error) {
        let alert = UIAlertController(title: nil, message: error.localizedDescription, preferredStyle: .alert)
        alert.addAction(.init(title: "OK", style: .cancel))
        navigation.present(alert, animated: true)
    }
}

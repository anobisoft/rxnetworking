
import RxSwift
import RxCocoa

class ErrorTracker {
    private let _errors: PublishSubject<Error>

    init() {
        self._errors = .init()
    }

    func track<O: ObservableConvertibleType>(_ source: O) -> Observable<O.Element> {
        return source.asObservable().catchError { error -> Observable<O.Element> in
            return .error(error)
        }.do(onError: on(error:))
    }

    func on(error: Error) {
        #if DEBUG
            print(error)
        #endif
        _errors.on(.next(error))
    }

    deinit {
        _errors.on(.completed)
    }
}

extension ErrorTracker: SharedSequenceConvertibleType {

    typealias Element = Error
    typealias SharingStrategy = SignalSharingStrategy
    func asSharedSequence() -> SharedSequence<SharingStrategy, Element> {
        return _errors.asSignal(onErrorSignalWith: .empty())
    }
}

extension ObservableConvertibleType {
    func trackError(_ tracker: ErrorTracker) -> Observable<Element> {
        return tracker.track(self)
    }
}

extension ObservableConvertibleType {
    func trackToSignal(_ tracker: ErrorTracker) -> Signal<Element> {
        return tracker.track(self).asSignal(onErrorSignalWith: .empty())
    }

    func trackToDriver(_ tracker: ErrorTracker) -> Driver<Element> {
        return tracker.track(self).asDriver(onErrorDriveWith: .empty())
    }
}

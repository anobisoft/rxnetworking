//
//  ServiceModel.swift
//  RxNetworking
//
//  Created by Stanislav Pletnev on 2020-07-27.
//  Copyright © 2020 Stanislav Pletnev. All rights reserved.
//

import Foundation

struct PasswordServiceItem: Codable {
    let id: UUID
    var name: String
    var description: String
    var url: URL
    var login: String
    var password: String
    var imageUrl: URL?
}

extension PasswordServiceItem {
    func changes(_ changed: Self) -> [String: String] {
        let mirror = Mirror(reflecting: self)
        let changedMirror = Mirror(reflecting: changed)
        var changedMap = [String: String]()
        for child in changedMirror.children {
            guard let label = child.label else { continue }
            changedMap[label] = "\(child.value)"
        }
        
        var result = [String: String]()
        for child in mirror.children {
            guard
                let label = child.label,
                let changedValue = changedMap[label]
                else {
                    continue
            }
            if "\(child.value)" != changedValue {
                result[label] = changedValue
            }
        }
        return result
    }
}

